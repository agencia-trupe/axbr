<?php

use Illuminate\Database\Seeder;

class PromocoesSeeder extends Seeder
{
    public function run()
    {
        DB::table('promocoes')->insert([
            'nacional_1' => '',
            'nacional_2' => '',
            'nacional_3' => '',
            'internacional_1' => '',
            'internacional_2' => '',
            'internacional_3' => '',
            'titulo_variavel' => '',
            'variavel_1' => '',
            'variavel_2' => '',
            'variavel_3' => '',
            'variavel_4' => '',
        ]);
    }
}
