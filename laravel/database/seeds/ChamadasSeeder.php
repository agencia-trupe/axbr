<?php

use Illuminate\Database\Seeder;

class ChamadasSeeder extends Seeder
{
    public function run()
    {
        DB::table('chamadas')->insert([
            'chamada_1_imagem' => '',
            'chamada_1_texto' => '',
            'chamada_1_link' => '',
            'chamada_2_imagem' => '',
            'chamada_2_texto' => '',
            'chamada_2_link' => '',
        ]);
    }
}
