<?php

use Illuminate\Database\Seeder;

class CambioSeeder extends Seeder
{
    public function run()
    {
        DB::table('cambio')->insert([
            'data' => '',
            'frase' => '',
            'dolar_americano_compra' => '',
            'dolar_americano_venda' => '',
            'euro_compra' => '',
            'euro_venda' => '',
            'dolar_australiano_compra' => '',
            'dolar_australiano_venda' => '',
            'dolar_canadense_compra' => '',
            'dolar_canadense_venda' => '',
            'dolar_neozelandes_compra' => '',
            'dolar_neozelandes_venda' => '',
            'peso_argentino_compra' => '',
            'peso_argentino_venda' => '',
            'peso_chileno_compra' => '',
            'peso_chileno_venda' => '',
            'peso_colombiano_compra' => '',
            'peso_colombiano_venda' => '',
            'peso_mexicano_compra' => '',
            'peso_mexicano_venda' => '',
            'peso_uruguaio_compra' => '',
            'peso_uruguaio_venda' => '',
            'libra_esterlina_compra' => '',
            'libra_esterlina_venda' => '',
            'franco_suisso_compra' => '',
            'franco_suisso_venda' => '',
            'novo_sol_peruano_compra' => '',
            'novo_sol_peruano_venda' => '',
            'iene_japones_compra' => '',
            'iene_japones_venda' => '',
        ]);
    }
}
