<?php

use Illuminate\Database\Seeder;

class ContatoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('contato')->insert([
            'email'                  => 'contato@axbrturismo.com.br',
            'telefones_cambio'       => '11 3805-0054, 11 99377-7392, 11 99377-7730',
            'telefones_turismo'      => '11 3805-0053, 11 99781-0373, 11 97660-8676',
            'endereco'               => 'Shopping Morumbi Town<br>Av. Giovanni Gronchi, 5930<br>Loja 35 - Piso Térreo - Hall Central<br>Vila Andrade - São Paulo<br>05724-002',
            'google_maps'            => '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3655.240460152593!2d-46.73988438502088!3d-23.63155808464805!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce514ffc1eed45%3A0x3d751c8832175d8b!2sAv.+Giovanni+Gronchi%2C+5930+-+Vila+Andrade%2C+S%C3%A3o+Paulo+-+SP%2C+05724-002!5e0!3m2!1spt-BR!2sbr!4v1515631618333" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>',
            'facebook'               => '#',
            'instagram'              => '#',
            'youtube'                => '#',
            'horario_de_atendimento' => 'Segunda a Sábado das 10h às 22h | Domingos das 14h às 20h<br>Fale conosco nos telefones e e-mail a qualquer hora!'
        ]);
    }
}
