<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePromocoesTable extends Migration
{
    public function up()
    {
        Schema::create('promocoes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('nacional_1');
            $table->integer('nacional_2');
            $table->integer('nacional_3');
            $table->integer('internacional_1');
            $table->integer('internacional_2');
            $table->integer('internacional_3');
            $table->string('titulo_variavel');
            $table->integer('variavel_1');
            $table->integer('variavel_2');
            $table->integer('variavel_3');
            $table->integer('variavel_4');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('promocoes');
    }
}
