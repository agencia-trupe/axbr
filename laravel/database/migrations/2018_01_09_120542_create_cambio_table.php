<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCambioTable extends Migration
{
    public function up()
    {
        Schema::create('cambio', function (Blueprint $table) {
            $table->increments('id');
            $table->string('data');
            $table->text('frase');
            $table->decimal('dolar_americano_compra', 10, 2);
            $table->decimal('dolar_americano_venda', 10, 2);
            $table->decimal('euro_compra', 10, 2);
            $table->decimal('euro_venda', 10, 2);
            $table->decimal('dolar_australiano_compra', 10, 2);
            $table->decimal('dolar_australiano_venda', 10, 2);
            $table->decimal('dolar_canadense_compra', 10, 2);
            $table->decimal('dolar_canadense_venda', 10, 2);
            $table->decimal('dolar_neozelandes_compra', 10, 2);
            $table->decimal('dolar_neozelandes_venda', 10, 2);
            $table->decimal('peso_argentino_compra', 10, 2);
            $table->decimal('peso_argentino_venda', 10, 2);
            $table->decimal('peso_chileno_compra', 10, 2);
            $table->decimal('peso_chileno_venda', 10, 2);
            $table->decimal('peso_colombiano_compra', 10, 2);
            $table->decimal('peso_colombiano_venda', 10, 2);
            $table->decimal('peso_mexicano_compra', 10, 2);
            $table->decimal('peso_mexicano_venda', 10, 2);
            $table->decimal('peso_uruguaio_compra', 10, 2);
            $table->decimal('peso_uruguaio_venda', 10, 2);
            $table->decimal('libra_esterlina_compra', 10, 2);
            $table->decimal('libra_esterlina_venda', 10, 2);
            $table->decimal('franco_suisso_compra', 10, 2);
            $table->decimal('franco_suisso_venda', 10, 2);
            $table->decimal('novo_sol_peruano_compra', 10, 2);
            $table->decimal('novo_sol_peruano_venda', 10, 2);
            $table->decimal('iene_japones_compra', 10, 2);
            $table->decimal('iene_japones_venda', 10, 2);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('cambio');
    }
}
