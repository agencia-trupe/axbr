<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContatoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contato', function (Blueprint $table) {
            $table->increments('id');
            $table->string('imagem');
            $table->string('telefones_cambio');
            $table->string('telefones_turismo');
            $table->string('email');
            $table->text('endereco');
            $table->text('google_maps');
            $table->string('facebook');
            $table->string('instagram');
            $table->string('youtube');
            $table->string('horario_de_atendimento');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('contato');
    }
}
