<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChamadasTable extends Migration
{
    public function up()
    {
        Schema::create('chamadas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('chamada_1_imagem');
            $table->text('chamada_1_texto');
            $table->string('chamada_1_link');
            $table->string('chamada_2_imagem');
            $table->text('chamada_2_texto');
            $table->string('chamada_2_link');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('chamadas');
    }
}
