<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCruzeirosTextoTable extends Migration
{
    public function up()
    {
        Schema::create('cruzeiros_texto', function (Blueprint $table) {
            $table->increments('id');
            $table->text('texto');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('cruzeiros_texto');
    }
}
