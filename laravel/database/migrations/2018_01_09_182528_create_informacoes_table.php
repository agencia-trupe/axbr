<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInformacoesTable extends Migration
{
    public function up()
    {
        Schema::create('informacoes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pacote_id')->unsigned();
            $table->integer('ordem')->default(0);
            $table->string('titulo');
            $table->string('descricao');
            $table->timestamps();
            $table->foreign('pacote_id')->references('id')->on('pacotes')->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::drop('informacoes');
    }
}
