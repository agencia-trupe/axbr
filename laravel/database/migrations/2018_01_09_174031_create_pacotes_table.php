<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePacotesTable extends Migration
{
    public function up()
    {
        Schema::create('pacotes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('slug');
            $table->string('continente');
            $table->string('pais');
            $table->string('titulo');
            $table->text('subtitulo');
            $table->string('capa');
            $table->text('texto');
            $table->timestamps();
        });

        Schema::create('pacotes_imagens', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pacote_id')->unsigned();
            $table->integer('ordem')->default(0);
            $table->string('imagem');
            $table->timestamps();
            $table->foreign('pacote_id')->references('id')->on('pacotes')->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::drop('pacotes_imagens');
        Schema::drop('pacotes');
    }
}
