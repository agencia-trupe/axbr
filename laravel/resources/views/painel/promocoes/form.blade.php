@include('painel.common.flash')

<p class="alert alert-info small" style="margin-bottom: 15px; height:45px; padding: 12px 15px;">
    <span class="glyphicon glyphicon-info-sign" style="margin-right:10px;"></span>
    Os quadros sem pacotes selecionados não serão exibidos no site.
</p>

<div class="well">
    {!! Form::label('', 'Promoções Nacionais') !!}
    <div class="row">
        <div class="col-md-4">
            <div class="form-group" style="margin-bottom:0">
                {!! Form::select('nacional_1', $pacotes, null, ['class' => 'form-control', 'placeholder' => 'Selecione']) !!}
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group" style="margin-bottom:0">
                {!! Form::select('nacional_2', $pacotes, null, ['class' => 'form-control', 'placeholder' => 'Selecione']) !!}
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group" style="margin-bottom:0">
                {!! Form::select('nacional_3', $pacotes, null, ['class' => 'form-control', 'placeholder' => 'Selecione']) !!}
            </div>
        </div>
    </div>
</div>

<hr>

<div class="well">
    {!! Form::label('', 'Promoções Internacionais') !!}
    <div class="row">
        <div class="col-md-4">
            <div class="form-group" style="margin-bottom:0">
                {!! Form::select('internacional_1', $pacotes, null, ['class' => 'form-control', 'placeholder' => 'Selecione']) !!}
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group" style="margin-bottom:0">
                {!! Form::select('internacional_2', $pacotes, null, ['class' => 'form-control', 'placeholder' => 'Selecione']) !!}
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group" style="margin-bottom:0">
                {!! Form::select('internacional_3', $pacotes, null, ['class' => 'form-control', 'placeholder' => 'Selecione']) !!}
            </div>
        </div>
    </div>
</div>

<hr>

<div class="well">
    {!! Form::text('titulo_variavel', null, ['class' => 'form-control', 'style' => 'margin-bottom:15px']) !!}
    <div class="row">
        <div class="col-md-3">
            <div class="form-group" style="margin-bottom: 0">
                {!! Form::select('variavel_1', $pacotes, null, ['class' => 'form-control', 'placeholder' => 'Selecione']) !!}
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group" style="margin-bottom: 0">
                {!! Form::select('variavel_2', $pacotes, null, ['class' => 'form-control', 'placeholder' => 'Selecione']) !!}
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group" style="margin-bottom: 0">
                {!! Form::select('variavel_3', $pacotes, null, ['class' => 'form-control', 'placeholder' => 'Selecione']) !!}
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group" style="margin-bottom: 0">
                {!! Form::select('variavel_4', $pacotes, null, ['class' => 'form-control', 'placeholder' => 'Selecione']) !!}
            </div>
        </div>
    </div>
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
