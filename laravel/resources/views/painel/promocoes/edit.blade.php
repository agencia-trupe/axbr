@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Promoções</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.promocoes.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.promocoes.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
