@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('continente', 'Continente') !!}
    {!! Form::select('continente', Tools::continentes(), null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('pais', 'País') !!}
    {!! Form::select('pais', Tools::paises(), null, ['class' => 'form-control', 'list' => 'paises']) !!}
</div>

<div class="form-group">
    {!! Form::label('titulo', 'Título') !!}
    {!! Form::text('titulo', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('subtitulo', 'Subtítulo') !!}
    {!! Form::textarea('subtitulo', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
</div>

<div class="well form-group">
    {!! Form::label('capa', 'Capa') !!}
@if($submitText == 'Alterar')
    <img src="{{ url('assets/img/pacotes/'.$registro->capa) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
@endif
    {!! Form::file('capa', ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('texto', 'Texto') !!}
    {!! Form::textarea('texto', null, ['class' => 'form-control ckeditor', 'data-editor' => 'texto']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.pacotes.index') }}" class="btn btn-default btn-voltar">Voltar</a>
