@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Pacotes /</small> Editar Pacote</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.pacotes.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.pacotes.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
