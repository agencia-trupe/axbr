@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Pacotes /</small> Adicionar Pacote</h2>
    </legend>

    {!! Form::open(['route' => 'painel.pacotes.store', 'files' => true]) !!}

        @include('painel.pacotes.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
