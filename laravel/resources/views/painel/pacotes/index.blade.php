@extends('painel.common.template')

@section('content')

    @include('painel.common.flash')

    <legend>
        <h2>
            Pacotes
            <a href="{{ route('painel.pacotes.create') }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Adicionar Pacote</a>
        </h2>
    </legend>


    @if(!count($registros))
    <div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
    @else
    <table class="table table-striped table-bordered table-hover table-info table-sortable" data-table="pacotes">
        <thead>
            <tr>
                {{--<th>Ordenar</th>--}}
                <th>Continente</th>
                <th>País</th>
                <th>Título</th>
                <th>Imagens</th>
                <th>Informações</th>
                <th class="no-filter"><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($registros as $registro)
            <tr class="tr-row" id="{{ $registro->id }}">
                {{--<td>
                    <a href="#" class="btn btn-info btn-sm btn-move">
                        <span class="glyphicon glyphicon-move"></span>
                    </a>
                </td>--}}
                <td>{{ Tools::continentes()[$registro->continente] }}</td>
                <td>{{ Tools::paises()[$registro->pais] }}</td>
                <td>{{ $registro->titulo }}</td>
                <td><a href="{{ route('painel.pacotes.imagens.index', $registro->id) }}" class="btn btn-info btn-sm">
                    <span class="glyphicon glyphicon-picture" style="margin-right:10px;"></span>Gerenciar
                </a></td>
                <td><a href="{{ route('painel.pacotes.informacoes.index', $registro->id) }}" class="btn btn-info btn-sm">
                    <span class="glyphicon glyphicon-th" style="margin-right:10px;"></span>Gerenciar
                </a></td>
                <td class="crud-actions">
                    {!! Form::open([
                        'route'  => ['painel.pacotes.destroy', $registro->id],
                        'method' => 'delete'
                    ]) !!}

                    <div class="btn-group btn-group-sm">
                        <a href="{{ route('painel.pacotes.edit', $registro->id ) }}" class="btn btn-primary btn-sm pull-left">
                            <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar
                        </a>

                        <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                    </div>

                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    @endif

@endsection
