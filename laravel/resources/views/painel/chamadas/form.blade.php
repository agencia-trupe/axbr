@include('painel.common.flash')

<div class="row">
    <div class="col-md-6">
       <div class="well form-group">
           {!! Form::label('chamada_1_imagem', 'Chamada 1 Imagem') !!}
           @if($registro->chamada_1_imagem)
           <img src="{{ url('assets/img/chamadas/'.$registro->chamada_1_imagem) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
           @endif
           {!! Form::file('chamada_1_imagem', ['class' => 'form-control']) !!}
       </div>

       <div class="form-group">
           {!! Form::label('chamada_1_texto', 'Chamada 1 Texto') !!}
           {!! Form::textarea('chamada_1_texto', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padraoBr']) !!}
       </div>

       <div class="form-group">
           {!! Form::label('chamada_1_link', 'Chamada 1 Link (opcional)') !!}
           {!! Form::text('chamada_1_link', null, ['class' => 'form-control']) !!}
       </div>
    </div>
    <div class="col-md-6">
        <div class="well form-group">
            {!! Form::label('chamada_2_imagem', 'Chamada 2 Imagem') !!}
            @if($registro->chamada_2_imagem)
            <img src="{{ url('assets/img/chamadas/'.$registro->chamada_2_imagem) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
            @endif
            {!! Form::file('chamada_2_imagem', ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('chamada_2_texto', 'Chamada 2 Texto') !!}
            {!! Form::textarea('chamada_2_texto', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padraoBr']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('chamada_2_link', 'Chamada 2 Link (opcional)') !!}
            {!! Form::text('chamada_2_link', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
