@extends('painel.common.template')

@section('content')

    <a href="{{ route('painel.cruzeiros.index') }}" title="Voltar para Cruzeiros" class="btn btn-sm btn-default">
        &larr; Voltar para Cruzeiros
    </a>

    <legend>
        <h2><small>Cruzeiros /</small> Texto</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.cruzeiros-texto.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.cruzeiros-texto.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
