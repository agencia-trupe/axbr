<ul class="nav navbar-nav">
	<li @if(str_is('painel.promocoes*', Route::currentRouteName())) class="active" @endif>
		<a href="{{ route('painel.promocoes.index') }}">Promoções</a>
	</li>
    <li @if(str_is('painel.chamadas*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.chamadas.index') }}">Chamadas</a>
    </li>
	<li @if(str_is('painel.pacotes*', Route::currentRouteName())) class="active" @endif>
		<a href="{{ route('painel.pacotes.index') }}">Pacotes</a>
	</li>
	<li @if(str_is('painel.cambio*', Route::currentRouteName())) class="active" @endif>
		<a href="{{ route('painel.cambio.index') }}">Câmbio</a>
	</li>
	<li @if(str_is('painel.cruzeiros*', Route::currentRouteName())) class="active" @endif>
		<a href="{{ route('painel.cruzeiros.index') }}">Cruzeiros</a>
	</li>
	<li @if(str_is('painel.servicos*', Route::currentRouteName())) class="active" @endif>
		<a href="{{ route('painel.servicos.index') }}">Serviços</a>
	</li>
    <li class="dropdown @if(str_is('painel.contato*', Route::currentRouteName())) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            Contato
            @if($contatosNaoLidos >= 1 || $cotacoesNaoLidas >= 1)
            <span class="label label-success" style="margin-left:3px;">{{ $contatosNaoLidos + $cotacoesNaoLidas }}</span>
            @endif
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <li @if(str_is('painel.contato.index', Route::currentRouteName())) class="active" @endif><a href="{{ route('painel.contato.index') }}">Informações de Contato</a></li>
            <li class="divider"></li>
            <li @if(str_is('painel.contato.recebidos*', Route::currentRouteName())) class="active" @endif><a href="{{ route('painel.contato.recebidos.index') }}">
                Contatos Recebidos
                @if($contatosNaoLidos >= 1)
                <span class="label label-success" style="margin-left:3px;">{{ $contatosNaoLidos }}</span>
                @endif
            </a></li>
            <li @if(str_is('painel.contato.cotacoes*', Route::currentRouteName())) class="active" @endif><a href="{{ route('painel.contato.cotacoes.index') }}">
                Contatos Recebidos - Cruzeiros
                @if($cotacoesNaoLidas >= 1)
                <span class="label label-success" style="margin-left:3px;">{{ $cotacoesNaoLidas }}</span>
                @endif
            </a></li>
        </ul>
    </li>
</ul>
