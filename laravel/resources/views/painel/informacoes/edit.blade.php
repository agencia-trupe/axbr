@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Pacotes / {{ $pacote->titulo }} /</small> Editar Informação</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.pacotes.informacoes.update', $pacote->id, $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.informacoes.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
