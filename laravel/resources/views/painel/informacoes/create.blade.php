@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Pacotes / {{ $pacote->titulo }} /</small> Adicionar Informação</h2>
    </legend>

    {!! Form::open(['route' => ['painel.pacotes.informacoes.store', $pacote->id], 'files' => true]) !!}

        @include('painel.informacoes.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
