@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Cruzeiros /</small> Editar Cruzeiro</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.cruzeiros.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.cruzeiros.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
