@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Cruzeiros /</small> Adicionar Cruzeiro</h2>
    </legend>

    {!! Form::open(['route' => 'painel.cruzeiros.store', 'files' => true]) !!}

        @include('painel.cruzeiros.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
