@include('painel.common.flash')

<div class="well form-group">
    {!! Form::label('imagem', 'Imagem') !!}
    @if($contato->imagem)
    <img src="{{ url('assets/img/contato/'.$contato->imagem) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    @endif
    {!! Form::file('imagem', ['class' => 'form-control']) !!}
</div>

<hr>

<div class="form-group">
    {!! Form::label('telefones_cambio', 'Telefones Câmbio (separados por vírgula)') !!}
    {!! Form::text('telefones_cambio', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('telefones_turismo', 'Telefones Turismo (separados por vírgula)') !!}
    {!! Form::text('telefones_turismo', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('email', 'E-mail') !!}
    {!! Form::email('email', null, ['class' => 'form-control']) !!}
</div>

<hr>

<div class="form-group">
    {!! Form::label('endereco', 'Endereço') !!}
    {!! Form::textarea('endereco', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
</div>

<div class="form-group">
    {!! Form::label('google_maps', 'Código GoogleMaps') !!}
    {!! Form::text('google_maps', null, ['class' => 'form-control']) !!}
</div>

<hr>

<div class="form-group">
    {!! Form::label('facebook', 'Facebook (opcional)') !!}
    {!! Form::text('facebook', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('instagram', 'Instagram (opcional)') !!}
    {!! Form::text('instagram', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('youtube', 'YouTube (opcional)') !!}
    {!! Form::text('youtube', null, ['class' => 'form-control']) !!}
</div>

<hr>

<div class="form-group">
    {!! Form::label('horario_de_atendimento', 'Horário de Atendimento na Loja') !!}
    {!! Form::textarea('horario_de_atendimento', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
