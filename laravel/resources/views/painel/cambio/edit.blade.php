@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Câmbio</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.cambio.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.cambio.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
