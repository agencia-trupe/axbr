@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('data', 'Data (Cotações válidas para)') !!}
    {!! Form::text('data', null, ['class' => 'form-control datepicker']) !!}
</div>

<div class="form-group">
    {!! Form::label('frase', 'Frase') !!}
    {!! Form::text('frase', null, ['class' => 'form-control']) !!}
</div>

<hr>

<div class="well">
    <div class="row">
        <div class="col-md-6">
            <div class="form-group" style="margin-bottom:0">
                {!! Form::label('dolar_americano_compra', 'Dólar Americano Compra') !!}
                {!! Form::number('dolar_americano_compra', null, ['class' => 'form-control', 'min' => '0', 'step' => '0.01']) !!}
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group" style="margin-bottom:0">
                {!! Form::label('dolar_americano_venda', 'Dólar Americano Venda') !!}
                {!! Form::number('dolar_americano_venda', null, ['class' => 'form-control', 'min' => '0', 'step' => '0.01']) !!}
            </div>
        </div>
    </div>
</div>

<div class="well">
    <div class="row">
        <div class="col-md-6">
            <div class="form-group" style="margin-bottom:0">
                {!! Form::label('euro_compra', 'Euro Compra') !!}
                {!! Form::number('euro_compra', null, ['class' => 'form-control', 'min' => '0', 'step' => '0.01']) !!}
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group" style="margin-bottom:0">
                {!! Form::label('euro_venda', 'Euro Venda') !!}
                {!! Form::number('euro_venda', null, ['class' => 'form-control', 'min' => '0', 'step' => '0.01']) !!}
            </div>
        </div>
    </div>
</div>

<div class="well">
    <div class="row">
        <div class="col-md-6">
            <div class="form-group" style="margin-bottom:0">
                {!! Form::label('dolar_australiano_compra', 'Dólar Australiano Compra') !!}
                {!! Form::number('dolar_australiano_compra', null, ['class' => 'form-control', 'min' => '0', 'step' => '0.01']) !!}
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group" style="margin-bottom:0">
                {!! Form::label('dolar_australiano_venda', 'Dólar Australiano Venda') !!}
                {!! Form::number('dolar_australiano_venda', null, ['class' => 'form-control', 'min' => '0', 'step' => '0.01']) !!}
            </div>
        </div>
    </div>
</div>

<div class="well">
    <div class="row">
        <div class="col-md-6">
            <div class="form-group" style="margin-bottom:0">
                {!! Form::label('dolar_canadense_compra', 'Dólar Canadense Compra') !!}
                {!! Form::number('dolar_canadense_compra', null, ['class' => 'form-control', 'min' => '0', 'step' => '0.01']) !!}
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group" style="margin-bottom:0">
                {!! Form::label('dolar_canadense_venda', 'Dólar Canadense Venda') !!}
                {!! Form::number('dolar_canadense_venda', null, ['class' => 'form-control', 'min' => '0', 'step' => '0.01']) !!}
            </div>
        </div>
    </div>
</div>

<div class="well">
    <div class="row">
        <div class="col-md-6">
            <div class="form-group" style="margin-bottom:0">
                {!! Form::label('dolar_neozelandes_compra', 'Dólar Neozelandês Compra') !!}
                {!! Form::number('dolar_neozelandes_compra', null, ['class' => 'form-control', 'min' => '0', 'step' => '0.01']) !!}
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group" style="margin-bottom:0">
                {!! Form::label('dolar_neozelandes_venda', 'Dólar Neozelandês Venda') !!}
                {!! Form::number('dolar_neozelandes_venda', null, ['class' => 'form-control', 'min' => '0', 'step' => '0.01']) !!}
            </div>
        </div>
    </div>
</div>

<div class="well">
    <div class="row">
        <div class="col-md-6">
            <div class="form-group" style="margin-bottom:0">
                {!! Form::label('peso_argentino_compra', 'Peso Argentino Compra') !!}
                {!! Form::number('peso_argentino_compra', null, ['class' => 'form-control', 'min' => '0', 'step' => '0.01']) !!}
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group" style="margin-bottom:0">
                {!! Form::label('peso_argentino_venda', 'Peso Argentino Venda') !!}
                {!! Form::number('peso_argentino_venda', null, ['class' => 'form-control', 'min' => '0', 'step' => '0.01']) !!}
            </div>
        </div>
    </div>
</div>

<div class="well">
    <div class="row">
        <div class="col-md-6">
            <div class="form-group" style="margin-bottom:0">
                {!! Form::label('peso_chileno_compra', 'Peso Chileno Compra') !!}
                {!! Form::number('peso_chileno_compra', null, ['class' => 'form-control', 'min' => '0', 'step' => '0.01']) !!}
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group" style="margin-bottom:0">
                {!! Form::label('peso_chileno_venda', 'Peso Chileno Venda') !!}
                {!! Form::number('peso_chileno_venda', null, ['class' => 'form-control', 'min' => '0', 'step' => '0.01']) !!}
            </div>
        </div>
    </div>
</div>

<div class="well">
    <div class="row">
        <div class="col-md-6">
            <div class="form-group" style="margin-bottom:0">
                {!! Form::label('peso_colombiano_compra', 'Peso Colombiano Compra') !!}
                {!! Form::number('peso_colombiano_compra', null, ['class' => 'form-control', 'min' => '0', 'step' => '0.01']) !!}
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group" style="margin-bottom:0">
                {!! Form::label('peso_colombiano_venda', 'Peso Colombiano Venda') !!}
                {!! Form::number('peso_colombiano_venda', null, ['class' => 'form-control', 'min' => '0', 'step' => '0.01']) !!}
            </div>
        </div>
    </div>
</div>

<div class="well">
    <div class="row">
        <div class="col-md-6">
            <div class="form-group" style="margin-bottom:0">
                {!! Form::label('peso_mexicano_compra', 'Peso Mexicano Compra') !!}
                {!! Form::number('peso_mexicano_compra', null, ['class' => 'form-control', 'min' => '0', 'step' => '0.01']) !!}
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group" style="margin-bottom:0">
                {!! Form::label('peso_mexicano_venda', 'Peso Mexicano Venda') !!}
                {!! Form::number('peso_mexicano_venda', null, ['class' => 'form-control', 'min' => '0', 'step' => '0.01']) !!}
            </div>
        </div>
    </div>
</div>

<div class="well">
    <div class="row">
        <div class="col-md-6">
            <div class="form-group" style="margin-bottom:0">
                {!! Form::label('peso_uruguaio_compra', 'Peso Uruguaio Compra') !!}
                {!! Form::number('peso_uruguaio_compra', null, ['class' => 'form-control', 'min' => '0', 'step' => '0.01']) !!}
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group" style="margin-bottom:0">
                {!! Form::label('peso_uruguaio_venda', 'Peso Uruguaio Venda') !!}
                {!! Form::number('peso_uruguaio_venda', null, ['class' => 'form-control', 'min' => '0', 'step' => '0.01']) !!}
            </div>
        </div>
    </div>
</div>

<div class="well">
    <div class="row">
        <div class="col-md-6">
            <div class="form-group" style="margin-bottom:0">
                {!! Form::label('libra_esterlina_compra', 'Libra Esterlina Compra') !!}
                {!! Form::number('libra_esterlina_compra', null, ['class' => 'form-control', 'min' => '0', 'step' => '0.01']) !!}
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group" style="margin-bottom:0">
                {!! Form::label('libra_esterlina_venda', 'Libra Esterlina Venda') !!}
                {!! Form::number('libra_esterlina_venda', null, ['class' => 'form-control', 'min' => '0', 'step' => '0.01']) !!}
            </div>
        </div>
    </div>
</div>

<div class="well">
    <div class="row">
        <div class="col-md-6">
            <div class="form-group" style="margin-bottom:0">
                {!! Form::label('franco_suisso_compra', 'Franco Suísso Compra') !!}
                {!! Form::number('franco_suisso_compra', null, ['class' => 'form-control', 'min' => '0', 'step' => '0.01']) !!}
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group" style="margin-bottom:0">
                {!! Form::label('franco_suisso_venda', 'Franco Suísso Venda') !!}
                {!! Form::number('franco_suisso_venda', null, ['class' => 'form-control', 'min' => '0', 'step' => '0.01']) !!}
            </div>
        </div>
    </div>
</div>

<div class="well">
    <div class="row">
        <div class="col-md-6">
            <div class="form-group" style="margin-bottom:0">
                {!! Form::label('novo_sol_peruano_compra', 'Novo Sol Peruano Compra') !!}
                {!! Form::number('novo_sol_peruano_compra', null, ['class' => 'form-control', 'min' => '0', 'step' => '0.01']) !!}
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group" style="margin-bottom:0">
                {!! Form::label('novo_sol_peruano_venda', 'Novo Sol Peruano Venda') !!}
                {!! Form::number('novo_sol_peruano_venda', null, ['class' => 'form-control', 'min' => '0', 'step' => '0.01']) !!}
            </div>
        </div>
    </div>
</div>

<div class="well">
    <div class="row">
        <div class="col-md-6">
            <div class="form-group" style="margin-bottom:0">
                {!! Form::label('iene_japones_compra', 'Iene Japonês Compra') !!}
                {!! Form::number('iene_japones_compra', null, ['class' => 'form-control', 'min' => '0', 'step' => '0.01']) !!}
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group" style="margin-bottom:0">
                {!! Form::label('iene_japones_venda', 'Iene Japonês Venda') !!}
                {!! Form::number('iene_japones_venda', null, ['class' => 'form-control', 'min' => '0', 'step' => '0.01']) !!}
            </div>
        </div>
    </div>
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
