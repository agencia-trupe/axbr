@extends('frontend.common.template')

@section('content')

    @include('frontend._cambio-include')

    <div class="destinos destinos-home">
        <div class="center">
            <h2>CONHEÇA NOSSOS DESTINOS</h2>
            <div>
                <a href="{{ route('turismo', 'america-do-norte') }}">
                    <div class="imagem">
                        <img src="{{ asset('assets/img/layout/continente-americanorte.png') }}" alt="">
                    </div>
                    <span>América do Norte</span>
                </a>
                <a href="{{ route('turismo', 'america-do-sul') }}">
                    <div class="imagem">
                        <img src="{{ asset('assets/img/layout/continente-americasul.png') }}" alt="">
                    </div>
                    <span>América do Sul</span>
                </a>
                <a href="{{ route('turismo', 'europa') }}">
                    <div class="imagem">
                        <img src="{{ asset('assets/img/layout/continente-europa.png') }}" alt="">
                    </div>
                    <span>Europa</span>
                </a>
                <a href="{{ route('turismo', 'asia') }}">
                    <div class="imagem">
                        <img src="{{ asset('assets/img/layout/continente-asia.png') }}" alt="">
                    </div>
                    <span>Ásia</span>
                </a>
                <a href="{{ route('turismo', 'africa') }}">
                    <div class="imagem">
                        <img src="{{ asset('assets/img/layout/continente-africa.png') }}" alt="">
                    </div>
                    <span>África</span>
                </a>
                <a href="{{ route('turismo', 'oceania') }}">
                    <div class="imagem">
                        <img src="{{ asset('assets/img/layout/continente-oceania.png') }}" alt="">
                    </div>
                    <span>Oceania</span>
                </a>
            </div>
        </div>
    </div>

    <div class="promocoes">
        <div class="center">
            @if(count($promocoes['internacionais']))
            <div class="promocoes-lista-padrao">
                <h2>PROMOÇÕES INTERNACIONAIS</h2>
                <div>
                    @foreach($promocoes['internacionais'] as $pacote)
                    <a href="{{ route('turismo', [$pacote->continente, $pacote->pais, $pacote->slug]) }}">
                        <img src="{{ asset('assets/img/pacotes/'.$pacote->capa) }}" alt="">
                        <div class="overlay">
                            <h3>{{ $pacote->titulo }}</h3>
                            <h4>{!! $pacote->subtitulo !!}</h4>
                        </div>
                    </a>
                    @endforeach
                </div>
            </div>
            @endif

            @if(count($promocoes['nacionais']))
            <div class="promocoes-lista-padrao">
                <h2>PROMOÇÕES NACIONAIS</h2>
                <div>
                    @foreach($promocoes['nacionais'] as $pacote)
                    <a href="{{ route('turismo', [$pacote->continente, $pacote->pais, $pacote->slug]) }}">
                        <img src="{{ asset('assets/img/pacotes/'.$pacote->capa) }}" alt="">
                        <div class="overlay">
                            <h3>{{ $pacote->titulo }}</h3>
                            <h4>{!! $pacote->subtitulo !!}</h4>
                        </div>
                    </a>
                    @endforeach
                </div>
            </div>
            @endif

            @if(count($promocoes['variaveis']))
            <div class="promocoes-lista-variavel">
                <h2>{{ $promocoes['variavelTitulo'] }}</h2>
                <div>
                    @foreach($promocoes['variaveis'] as $pacote)
                    <a href="{{ route('turismo', [$pacote->continente, $pacote->pais, $pacote->slug]) }}">
                        <img src="{{ asset('assets/img/pacotes/'.$pacote->capa) }}" alt="">
                        <div class="overlay">
                            <span>{{ $pacote->titulo }}</span>
                        </div>
                    </a>
                    @endforeach
                </div>
            </div>
            @endif
        </div>
    </div>

    <div class="cruzeiros-home">
        <div class="center">
            <h2>CRUZEIROS</h2>
            <div>
                @foreach($cruzeiros as $cruzeiro)
                <a href="{{ route('turismo.cruzeiros') }}">
                    <img src="{{ asset('assets/img/cruzeiros/'.$cruzeiro->imagem) }}" alt="">
                    <div class="overlay">
                        <span>{{ $cruzeiro->titulo }}</span>
                    </div>
                </a>
                @endforeach
            </div>
        </div>
    </div>

    <div class="servicos servicos-home">
        <div class="center">
            <h2>SERVIÇOS</h2>

            <div class="servicos-lista">
                @foreach($servicos as $servico)
                <a href="{{ route('turismo.servicos') }}" class="servico">
                    <img src="{{ asset('assets/img/servicos/'.$servico->imagem) }}" alt="">
                    <h3>{{ $servico->titulo }}</h3>
                </a>
                @endforeach
            </div>
        </div>
    </div>

@endsection
