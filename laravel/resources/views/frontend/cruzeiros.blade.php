@extends('frontend.common.template')

@section('content')

    <div class="destinos">
        <div class="center">
            <h2>CONHEÇA NOSSOS DESTINOS</h2>
            <div>
                <a href="{{ route('turismo', 'america-do-norte') }}">
                    <div class="imagem">
                        <img src="{{ asset('assets/img/layout/continente-americanorte.png') }}" alt="">
                    </div>
                    <span>América do Norte</span>
                </a>
                <a href="{{ route('turismo', 'america-do-sul') }}">
                    <div class="imagem">
                        <img src="{{ asset('assets/img/layout/continente-americasul.png') }}" alt="">
                    </div>
                    <span>América do Sul</span>
                </a>
                <a href="{{ route('turismo', 'europa') }}">
                    <div class="imagem">
                        <img src="{{ asset('assets/img/layout/continente-europa.png') }}" alt="">
                    </div>
                    <span>Europa</span>
                </a>
                <a href="{{ route('turismo', 'asia') }}">
                    <div class="imagem">
                        <img src="{{ asset('assets/img/layout/continente-asia.png') }}" alt="">
                    </div>
                    <span>Ásia</span>
                </a>
                <a href="{{ route('turismo', 'africa') }}">
                    <div class="imagem">
                        <img src="{{ asset('assets/img/layout/continente-africa.png') }}" alt="">
                    </div>
                    <span>África</span>
                </a>
                <a href="{{ route('turismo', 'oceania') }}">
                    <div class="imagem">
                        <img src="{{ asset('assets/img/layout/continente-oceania.png') }}" alt="">
                    </div>
                    <span>Oceania</span>
                </a>
            </div>
        </div>
    </div>

    <div class="cruzeiros">
        <div class="center">
            <h2>CRUZEIROS</h2>

            <div class="thumbs">
                @foreach($cruzeiros as $cruzeiro)
                <div>
                    <img src="{{ asset('assets/img/cruzeiros/'.$cruzeiro->imagem) }}" alt="">
                    <span>{{ $cruzeiro->titulo }}</span>
                </div>
                @endforeach
            </div>

            <div class="texto">
                {!! $texto->texto !!}
            </div>

            <div class="formulario">
                @if(session('success'))
                <div class="enviado">
                    Mensagem enviada com sucesso!
                </div>
                @else
                <form action="{{ route('turismo.cruzeiros-post') }}" method="POST">
                    {!! csrf_field() !!}

                    <p>PEÇA JÁ SUA COTAÇÃO</p>

                    @if($errors->any())
                    <div class="erro">Preencha todos os campos corretamente.</div>
                    @endif

                    <input type="text" name="nome" placeholder="Nome" required value="{{ old('nome') }}">
                    <input type="email" name="email" placeholder="E-mail" required value="{{ old('email') }}">
                    <input type="text" name="telefone" placeholder="Telefone" value="{{ old('telefone') }}">
                    <textarea name="mensagem" placeholder="Mensagem" required>{{ old('mensagem') }}</textarea>
                    <input type="submit" value="ENVIAR">
                </form>
                @endif
            </div>
        </div>
    </div>

    <div class="servicos servicos-cruzeiros">
        <div class="center">
            <h2>SERVIÇOS</h2>

            <div class="servicos-lista">
                @foreach($servicos as $servico)
                <a href="{{ route('turismo.servicos') }}" class="servico">
                    <img src="{{ asset('assets/img/servicos/'.$servico->imagem) }}" alt="">
                    <h3>{{ $servico->titulo }}</h3>
                </a>
                @endforeach
            </div>
        </div>
    </div>

@endsection
