@extends('frontend.common.template')

@section('content')

    <div class="contato">
        <div class="center">
            <div class="formulario">
                @if(session('success'))
                <div class="enviado">
                    Mensagem enviada com sucesso!
                </div>
                @else
                <form action="{{ route('contato.post') }}" method="POST">
                    {!! csrf_field() !!}

                    <p>PEÇA JÁ SUA COTAÇÃO</p>

                    @if($errors->any())
                    <div class="erro">Preencha todos os campos corretamente.</div>
                    @endif

                    <input type="text" name="nome" placeholder="Nome" required value="{{ old('nome') }}">
                    <input type="email" name="email" placeholder="E-mail" required value="{{ old('email') }}">
                    <input type="text" name="telefone" placeholder="Telefone" value="{{ old('telefone') }}">
                    <textarea name="mensagem" placeholder="Mensagem" required>{{ old('mensagem') }}</textarea>
                    <input type="submit" value="ENVIAR">
                </form>
                @endif
            </div>

            <div class="informacoes">
                <div class="telefones">
                    <div class="col">
                        <h3>CÂMBIO:</h3>
                        <p>
                            @foreach(explode(',', $contato->telefones_cambio) as $telefone)
                            <?php $telefone = explode(' ', trim($telefone)); ?>
                            <span>{{ array_shift($telefone) }} <strong>{{ implode(' ', $telefone) }}</strong></span>
                            @endforeach
                            <a href="mailto:{{ $contato->email }}">{{ $contato->email }}</a>
                        </p>
                    </div>
                    <div class="col">
                        <h3>TURISMO:</h3>
                        <p>
                            @foreach(explode(',', $contato->telefones_cambio) as $telefone)
                            <?php $telefone = explode(' ', trim($telefone)); ?>
                            <span>{{ array_shift($telefone) }} <strong>{{ implode(' ', $telefone) }}</strong></span>
                            @endforeach
                        </p>
                    </div>
                </div>
                <div class="atendimento">
                    <h4>HORÁRIO DE ATENDIMENTO NA LOJA:</h4>
                    <p>{!! $contato->horario_de_atendimento !!}</p>
                </div>

                <div class="localizacao">
                    <h4>LOCALIZAÇÃO:</h4>
                    <p>{!! $contato->endereco !!}</p>
                </div>
            </div>
        </div>
    </div>

@endsection
