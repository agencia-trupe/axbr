<div class="cambio @if(Route::currentRouteName() == 'home') cambio-home @endif">
    <div class="center">
        <p class="frase">
            Cotações válidas para <strong>{{ $cambio->data }}</strong>.
            {{ $cambio->frase }}
            <span class="iof">IOF INCLUSO.</span>
        </p>

        <div class="cotacoes">
            <div class="dolar-americano" data-prefixo="USD $" data-compra="{{ $cambio->dolar_americano_compra }}" data-venda="{{ $cambio->dolar_americano_venda }}" data-plural="dólares americanos">
                <img src="{{ asset('assets/img/layout/balao-dolar.png') }}" alt="">
                <div class="texto">
                    <h4>DÓLAR AMERICANO</h4>
                    <p>
                        <span class="compra">COMPRA:</span> R$ <strong>{{ str_replace('.', ',', $cambio->dolar_americano_compra) }}</strong>
                    </p>
                    <p>
                        <span class="venda">VENDA:</span> R$ <strong>{{ str_replace('.', ',', $cambio->dolar_americano_venda) }}</strong>
                    </p>
                </div>
            </div>
            <div class="euro" data-prefixo="EUR €" data-compra="{{ $cambio->euro_compra }}" data-venda="{{ $cambio->euro_venda }}" data-plural="euros">
                <img src="{{ asset('assets/img/layout/balao-euro.png') }}" alt="">
                <div class="texto">
                    <h4>EURO</h4>
                    <p>
                        <span class="compra">COMPRA:</span> R$ <strong>{{ str_replace('.', ',', $cambio->euro_compra) }}</strong>
                    </p>
                    <p>
                        <span class="venda">VENDA:</span> R$ <strong>{{ str_replace('.', ',', $cambio->euro_venda) }}</strong>
                    </p>
                </div>
            </div>
            <div class="dolar-australiano" data-prefixo="AUD $" data-compra="{{ $cambio->dolar_australiano_compra }}" data-venda="{{ $cambio->dolar_australiano_venda }}" data-plural="dólares australianos">
                <img src="{{ asset('assets/img/layout/balao-dolaraustraliano.png') }}" alt="">
                <div class="texto">
                    <h4>DÓLAR AUSTRALIANO</h4>
                    <p>
                        <span class="compra">COMPRA:</span> R$ <strong>{{ str_replace('.', ',', $cambio->dolar_australiano_compra) }}</strong>
                    </p>
                    <p>
                        <span class="venda">VENDA:</span> R$ <strong>{{ str_replace('.', ',', $cambio->dolar_australiano_venda) }}</strong>
                    </p>
                </div>
            </div>
            <div class="dolar-canadense" data-prefixo="CAD $" data-compra="{{ $cambio->dolar_canadense_compra }}" data-venda="{{ $cambio->dolar_canadense_venda }}" data-plural="dólares canadenses">
                <img src="{{ asset('assets/img/layout/balao-dolarcanadense.png') }}" alt="">
                <div class="texto">
                    <h4>DÓLAR CANADENSE</h4>
                    <p>
                        <span class="compra">COMPRA:</span> R$ <strong>{{ str_replace('.', ',', $cambio->dolar_canadense_compra) }}</strong>
                    </p>
                    <p>
                        <span class="venda">VENDA:</span> R$ <strong>{{ str_replace('.', ',', $cambio->dolar_canadense_venda) }}</strong>
                    </p>
                </div>
            </div>
            <div class="dolar-neozelandes" data-prefixo="NZD $" data-compra="{{ $cambio->dolar_neozelandes_compra }}" data-venda="{{ $cambio->dolar_neozelandes_venda }}" data-plural="dólares neozelandeses">
                <img src="{{ asset('assets/img/layout/balao-dolarneozelandes.png') }}" alt="">
                <div class="texto">
                    <h4>DÓLAR NEOZELANDÊS</h4>
                    <p>
                        <span class="compra">COMPRA:</span> R$ <strong>{{ str_replace('.', ',', $cambio->dolar_neozelandes_compra) }}</strong>
                    </p>
                    <p>
                        <span class="venda">VENDA:</span> R$ <strong>{{ str_replace('.', ',', $cambio->dolar_neozelandes_venda) }}</strong>
                    </p>
                </div>
            </div>
            <div class="peso-argentino" data-prefixo="ARS $" data-compra="{{ $cambio->peso_argentino_compra }}" data-venda="{{ $cambio->peso_argentino_venda }}" data-plural="pesos argentinos">
                <img src="{{ asset('assets/img/layout/balao-pesoargentino.png') }}" alt="">
                <div class="texto">
                    <h4>PESO ARGENTINO</h4>
                    <p>
                        <span class="compra">COMPRA:</span> R$ <strong>{{ str_replace('.', ',', $cambio->peso_argentino_compra) }}</strong>
                    </p>
                    <p>
                        <span class="venda">VENDA:</span> R$ <strong>{{ str_replace('.', ',', $cambio->peso_argentino_venda) }}</strong>
                    </p>
                </div>
            </div>
            <div class="peso-chileno" data-prefixo="CLP $" data-compra="{{ $cambio->peso_chileno_compra }}" data-venda="{{ $cambio->peso_chileno_venda }}" data-plural="pesos chilenos">
                <img src="{{ asset('assets/img/layout/balao-pesochileno.png') }}" alt="">
                <div class="texto">
                    <h4>PESO CHILENO</h4>
                    <p>
                        <span class="compra">COMPRA:</span> R$ <strong>{{ str_replace('.', ',', $cambio->peso_chileno_compra) }}</strong>
                    </p>
                    <p>
                        <span class="venda">VENDA:</span> R$ <strong>{{ str_replace('.', ',', $cambio->peso_chileno_venda) }}</strong>
                    </p>
                </div>
            </div>
            <div class="peso-colombiano" data-prefixo="COP $" data-compra="{{ $cambio->peso_colombiano_compra }}" data-venda="{{ $cambio->peso_colombiano_venda }}" data-plural="pesos colombianos">
                <img src="{{ asset('assets/img/layout/balao-pesocolombiano.png') }}" alt="">
                <div class="texto">
                    <h4>PESO COLOMBIANO</h4>
                    <p>
                        <span class="compra">COMPRA:</span> R$ <strong>{{ str_replace('.', ',', $cambio->peso_colombiano_compra) }}</strong>
                    </p>
                    <p>
                        <span class="venda">VENDA:</span> R$ <strong>{{ str_replace('.', ',', $cambio->peso_colombiano_venda) }}</strong>
                    </p>
                </div>
            </div>
            <div class="peso-mexicano" data-prefixo="MXN $" data-compra="{{ $cambio->peso_mexicano_compra }}" data-venda="{{ $cambio->peso_mexicano_venda }}" data-plural="pesos mexicanos">
                <img src="{{ asset('assets/img/layout/balao-pesomexicano.png') }}" alt="">
                <div class="texto">
                    <h4>PESO MEXICANO</h4>
                    <p>
                        <span class="compra">COMPRA:</span> R$ <strong>{{ str_replace('.', ',', $cambio->peso_mexicano_compra) }}</strong>
                    </p>
                    <p>
                        <span class="venda">VENDA:</span> R$ <strong>{{ str_replace('.', ',', $cambio->peso_mexicano_venda) }}</strong>
                    </p>
                </div>
            </div>
            <div class="peso-uruguaio" data-prefixo="UYU" data-compra="{{ $cambio->peso_uruguaio_compra }}" data-venda="{{ $cambio->peso_uruguaio_venda }}" data-plural="pesos uruguaios">
                <img src="{{ asset('assets/img/layout/balao-pesouruguaio.png') }}" alt="">
                <div class="texto">
                    <h4>PESO URUGUAIO</h4>
                    <p>
                        <span class="compra">COMPRA:</span> R$ <strong>{{ str_replace('.', ',', $cambio->peso_uruguaio_compra) }}</strong>
                    </p>
                    <p>
                        <span class="venda">VENDA:</span> R$ <strong>{{ str_replace('.', ',', $cambio->peso_uruguaio_venda) }}</strong>
                    </p>
                </div>
            </div>
            <div class="libra-esterlina" data-prefixo="£" data-compra="{{ $cambio->libra_esterlina_compra }}" data-venda="{{ $cambio->libra_esterlina_venda }}" data-plural="libras esterlinas">
                <img src="{{ asset('assets/img/layout/balao-libraesterlina.png') }}" alt="">
                <div class="texto">
                    <h4>LIBRA ESTERLINA</h4>
                    <p>
                        <span class="compra">COMPRA:</span> R$ <strong>{{ str_replace('.', ',', $cambio->libra_esterlina_compra) }}</strong>
                    </p>
                    <p>
                        <span class="venda">VENDA:</span> R$ <strong>{{ str_replace('.', ',', $cambio->libra_esterlina_venda) }}</strong>
                    </p>
                </div>
            </div>
            <div class="franco-suisso" data-prefixo="Fr" data-compra="{{ $cambio->franco_suisso_compra }}" data-venda="{{ $cambio->franco_suisso_venda }}" data-plural="francos súissos">
                <img src="{{ asset('assets/img/layout/balao-francosuisso.png') }}" alt="">
                <div class="texto">
                    <h4>FRANCO SUÍSSO</h4>
                    <p>
                        <span class="compra">COMPRA:</span> R$ <strong>{{ str_replace('.', ',', $cambio->franco_suisso_compra) }}</strong>
                    </p>
                    <p>
                        <span class="venda">VENDA:</span> R$ <strong>{{ str_replace('.', ',', $cambio->franco_suisso_venda) }}</strong>
                    </p>
                </div>
            </div>
            <div class="novo-sol-peruano" data-prefixo="S/." data-compra="{{ $cambio->novo_sol_peruano_compra }}" data-venda="{{ $cambio->novo_sol_peruano_venda }}" data-plural="novos sóis peruanos">
                <img src="{{ asset('assets/img/layout/balao-solperuano.png') }}" alt="">
                <div class="texto">
                    <h4>NOVO SOL PERUANO</h4>
                    <p>
                        <span class="compra">COMPRA:</span> R$ <strong>{{ str_replace('.', ',', $cambio->novo_sol_peruano_compra) }}</strong>
                    </p>
                    <p>
                        <span class="venda">VENDA:</span> R$ <strong>{{ str_replace('.', ',', $cambio->novo_sol_peruano_venda) }}</strong>
                    </p>
                </div>
            </div>
            <div class="iene-japones" data-prefixo="¥" data-compra="{{ $cambio->iene_japones_compra }}" data-venda="{{ $cambio->iene_japones_venda }}" data-plural="ienes japoneses">
                <img src="{{ asset('assets/img/layout/balao-iene.png') }}" alt="">
                <div class="texto">
                    <h4>IENE JAPONÊS</h4>
                    <p>
                        <span class="compra">COMPRA:</span> R$ <strong>{{ str_replace('.', ',', $cambio->iene_japones_compra) }}</strong>
                    </p>
                    <p>
                        <span class="venda">VENDA:</span> R$ <strong>{{ str_replace('.', ',', $cambio->iene_japones_venda) }}</strong>
                    </p>
                </div>
            </div>
        </div>

        <div class="bloco">
            <div class="chamadas">
                @foreach(range(1, 2) as $i)
                    @if($chamadas->{'chamada_'.$i.'_link'})
                    <a class="chamada" href="{{ $chamadas->{'chamada_'.$i.'_link'} }}">
                        <img src="{{ asset('assets/img/chamadas/'.$chamadas->{'chamada_'.$i.'_imagem'}) }}" alt="">
                        <div class="texto">
                            {!! $chamadas->{'chamada_'.$i.'_texto'} !!}
                        </div>
                    </a>
                    @else
                    <div class="chamada">
                        <img src="{{ asset('assets/img/chamadas/'.$chamadas->{'chamada_'.$i.'_imagem'}) }}" alt="">
                        <div class="texto">
                            {!! $chamadas->{'chamada_'.$i.'_texto'} !!}
                        </div>
                    </div>
                    @endif
                @endforeach
            </div>

            <div class="calculadora">
                <h3>CALCULADORA</h3>
                <div class="formulario">
                    <p>Calcule a quantidade de moeda correspondente ao valor em reais (para compra):</p>
                    <label>
                        <span>VALOR EM REAIS:</span>
                        <input type="text" name="reais" class="mascara-reais">
                    </label>
                    <label>
                        <span>MOEDA:</span>
                        <select name="moeda">
                            <option value="">Selecione...</option>
                            <option value="dolar-americano">Dólar Americano</option>
                            <option value="euro">Euro</option>
                            <option value="dolar-australiano">Dólar Australiano</option>
                            <option value="dolar-canadense">Dólar Canadense</option>
                            <option value="dolar-neozelandes">Dólar Neozelandês</option>
                            <option value="peso-argentino">Peso Argentino</option>
                            <option value="peso-chileno">Peso Chileno</option>
                            <option value="peso-colombiano">Peso Colombiano</option>
                            <option value="peso-mexicano">Peso Mexicano</option>
                            <option value="peso-uruguaio">Peso Uruguaio</option>
                            <option value="libra-esterlina">Libra Esterlina</option>
                            <option value="franco-suisso">Franco Suísso</option>
                            <option value="novo-sol-peruano">Novo Sol Peruano</option>
                            <option value="iene-japones">Iene Japonês</option>
                        </select>
                    </label>
                    <input type="submit" value="CALCULAR">
                </div>
                <div class="resultado" style="display:none">
                    <p>Confira o resultado:</p>
                    <p class="resultado-texto"></p>
                    <a href="#" class="voltar">VOLTAR</a>
                </div>
            </div>
        </div>
    </div>
</div>
