    <header>
        <div class="center">
            <nav class="desktop">
                <a href="{{ route('home') }}" @if(Route::currentRouteName() == 'home') class="active" @endif>HOME</a>
                <a href="{{ route('turismo') }}" @if(str_is('turismo*', Route::currentRouteName())) class="active" @endif>TURISMO</a>
                <a href="{{ route('home') }}" class="logo"><img src="{{ asset('assets/img/layout/marca-axbr.png') }}" alt=""></a>
                <a href="{{ route('cambio') }}" @if(Route::currentRouteName() == 'cambio') class="active" @endif>CÂMBIO</a>
                <a href="{{ route('contato') }}" @if(Route::currentRouteName() == 'contato') class="active" @endif>CONTATO</a>
            </nav>
            <nav class="mobile">
                <a href="{{ route('home') }}" class="logo"><img src="{{ asset('assets/img/layout/marca-axbr.png') }}" alt=""></a>
                <a href="{{ route('home') }}" @if(Route::currentRouteName() == 'home') class="active" @endif>HOME</a>
                <a href="{{ route('turismo') }}" @if(str_is('turismo*', Route::currentRouteName())) class="active" @endif>TURISMO</a>
                <a href="{{ route('cambio') }}" @if(Route::currentRouteName() == 'cambio') class="active" @endif>CÂMBIO</a>
                <a href="{{ route('contato') }}" @if(Route::currentRouteName() == 'contato') class="active" @endif>CONTATO</a>
            </nav>
            <div class="social">
                @foreach(['facebook', 'instagram', 'youtube'] as $s)
                    @if($contato->{$s})
                    <a href="{{ $contato->{$s} }}" class="{{ $s }}">{{ $s }}</a>
                    @endif
                @endforeach
            </div>
        </div>
    </header>
