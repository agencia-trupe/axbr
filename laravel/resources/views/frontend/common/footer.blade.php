    <footer>
        <div class="informacoes">
            <div class="center">
                <div class="links">
                    <img src="{{ asset('assets/img/layout/marca-axbr-rodape.png') }}" alt="">
                    <a href="{{ route('home') }}">&raquo; HOME</a>
                    <a href="{{ route('cambio') }}">&raquo; CÂMBIO</a>
                    <a href="{{ route('turismo') }}">&raquo; TURISMO</a>
                    <a href="{{ route('contato') }}">&raquo; CONTATO</a>
                </div>
                <div class="telefones">
                    <h3>CÂMBIO:</h3>
                    <p>
                        @foreach(explode(',', $contato->telefones_cambio) as $telefone)
                        <?php $telefone = explode(' ', trim($telefone)); ?>
                        <span>{{ array_shift($telefone) }} <strong>{{ implode(' ', $telefone) }}</strong></span>
                        @endforeach
                    </p>
                </div>
                <div class="endereco desktop">
                    <img src="{{ asset('assets/img/contato/'.$contato->imagem) }}" alt="">
                    <p>{!! $contato->endereco !!}</p>
                </div>
                <div class="telefones">
                    <h3>TURISMO:</h3>
                    <p>
                        @foreach(explode(',', $contato->telefones_cambio) as $telefone)
                        <?php $telefone = explode(' ', trim($telefone)); ?>
                        <span>{{ array_shift($telefone) }} <strong>{{ implode(' ', $telefone) }}</strong></span>
                        @endforeach
                        <a href="mailto:{{ $contato->email }}">{{ $contato->email }}</a>
                    </p>
                </div>
                <div class="endereco mobile">
                    <img src="{{ asset('assets/img/contato/'.$contato->imagem) }}" alt="">
                    <p>{!! $contato->endereco !!}</p>
                </div>
                <div class="mapa">{!! $contato->google_maps !!}</div>
            </div>
        </div>
        <div class="copyright">
            <p>© {{ date('Y') }} {{ $config->title }} - Todos os direitos reservados.</p>
        </div>
    </footer>
