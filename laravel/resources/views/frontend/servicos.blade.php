@extends('frontend.common.template')

@section('content')

    <div class="destinos">
        <div class="center">
            <h2>CONHEÇA NOSSOS DESTINOS</h2>
            <div>
                <a href="{{ route('turismo', 'america-do-norte') }}">
                    <div class="imagem">
                        <img src="{{ asset('assets/img/layout/continente-americanorte.png') }}" alt="">
                    </div>
                    <span>América do Norte</span>
                </a>
                <a href="{{ route('turismo', 'america-do-sul') }}">
                    <div class="imagem">
                        <img src="{{ asset('assets/img/layout/continente-americasul.png') }}" alt="">
                    </div>
                    <span>América do Sul</span>
                </a>
                <a href="{{ route('turismo', 'europa') }}">
                    <div class="imagem">
                        <img src="{{ asset('assets/img/layout/continente-europa.png') }}" alt="">
                    </div>
                    <span>Europa</span>
                </a>
                <a href="{{ route('turismo', 'asia') }}">
                    <div class="imagem">
                        <img src="{{ asset('assets/img/layout/continente-asia.png') }}" alt="">
                    </div>
                    <span>Ásia</span>
                </a>
                <a href="{{ route('turismo', 'africa') }}">
                    <div class="imagem">
                        <img src="{{ asset('assets/img/layout/continente-africa.png') }}" alt="">
                    </div>
                    <span>África</span>
                </a>
                <a href="{{ route('turismo', 'oceania') }}">
                    <div class="imagem">
                        <img src="{{ asset('assets/img/layout/continente-oceania.png') }}" alt="">
                    </div>
                    <span>Oceania</span>
                </a>
            </div>
        </div>
    </div>

    <div class="servicos">
        <div class="center">
            <h2>SERVIÇOS</h2>

            <div class="servicos-lista">
                @foreach($servicos as $servico)
                <div class="servico">
                    <img src="{{ asset('assets/img/servicos/'.$servico->imagem) }}" alt="">
                    <h3>{{ $servico->titulo }}</h3>
                    {!! $servico->texto !!}
                </div>
                @endforeach
            </div>

            <a href="{{ route('contato') }}" class="servicos-contato">SOLICITE O SEU SERVIÇO AGORA</a>
        </div>
    </div>

@endsection
