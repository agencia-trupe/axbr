import AjaxSetup from './AjaxSetup';
import MobileToggle from './MobileToggle';

AjaxSetup();
MobileToggle();

$('.mascara-reais').inputmask('decimal', {
    radixPoint:",",
    groupSeparator: ".",
    autoGroup: true,
    digits: 2,
    digitsOptional: false,
    placeholder: '0',
    prefix: 'R$ ',
    rightAlign: false,
});

$('.calculadora input[type=submit]').click(function(event) {
    event.preventDefault();

    var reais = parseFloat($('.mascara-reais').inputmask('unmaskedvalue').replace(',', '.'));

    if (!reais) {
        return alert('Preencha o valor em reais.');
    }

    var moedaSlug = $('select[name=moeda] :selected').val();

    if (!moedaSlug) {
        return alert('Selecione uma moeda.');
    }

    var $moeda = $('.' + moedaSlug);

    var moedaCompra  = parseFloat($moeda.data('compra'));
    var moedaPrefixo = $moeda.data('prefixo');
    var moedaPlural  = $moeda.data('plural');

    if (reais < moedaCompra) {
        return alert('Valor em reais insuficiente.');
    }

    var resultado = `
        R$ ${reais.toFixed(2).toString().replace('.', ',')} compram:
        <strong>${moedaPrefixo} ${(reais / moedaCompra).toFixed(2).toString().replace('.', ',')}</strong>
        (${moedaPlural})
    `;

    $('.resultado-texto').html(resultado);

    $('.formulario').hide();
    $('.resultado').show();
});

$('.voltar').click(function(event) {
    event.preventDefault();

    $('input[name=reais]').val('');
    $('select[name=moeda] :selected').removeAttr('selected');

    $('.formulario').show();
    $('.resultado').hide();

    $('.resultado-texto').html('');
});
