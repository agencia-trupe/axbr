<?php

namespace App\Providers;

use Illuminate\Routing\Router;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to the controller routes in your routes file.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function boot(Router $router)
    {
		$router->model('promocoes', 'App\Models\Promocoes');
		$router->model('informacoes', 'App\Models\Informacao');
		$router->model('pacotes', 'App\Models\Pacote');
		$router->model('imagens_pacotes', 'App\Models\PacoteImagem');
		$router->model('chamadas', 'App\Models\Chamadas');
		$router->model('cambio', 'App\Models\Cambio');
		$router->model('cruzeiros-texto', 'App\Models\CruzeirosTexto');
		$router->model('cruzeiros', 'App\Models\Cruzeiro');
		$router->model('servicos', 'App\Models\Servico');
		$router->model('configuracoes', 'App\Models\Configuracoes');
        $router->model('recebidos', 'App\Models\ContatoRecebido');
        $router->model('cotacoes', 'App\Models\CotacaoRecebida');
        $router->model('contato', 'App\Models\Contato');
        $router->model('usuarios', 'App\Models\User');

        parent::boot($router);
    }

    /**
     * Define the routes for the application.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function map(Router $router)
    {
        $router->group(['namespace' => $this->namespace], function ($router) {
            require app_path('Http/routes.php');
        });
    }
}
