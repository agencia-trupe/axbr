<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Cruzeiro extends Model
{
    protected $table = 'cruzeiros';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public static function upload_imagem()
    {
        return CropImage::make('imagem', [
            'width'  => 287,
            'height' => 180,
            'path'   => 'assets/img/cruzeiros/'
        ]);
    }
}
