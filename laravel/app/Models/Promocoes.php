<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Promocoes extends Model
{
    protected $table = 'promocoes';

    protected $guarded = ['id'];

}
