<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class PacoteImagem extends Model
{
    protected $table = 'pacotes_imagens';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function scopePacote($query, $id)
    {
        return $query->where('pacote_id', $id);
    }

    public static function uploadImagem()
    {
        return CropImage::make('imagem', [
            [
                'width'   => 180,
                'height'  => 180,
                'path'    => 'assets/img/pacotes/imagens/thumbs/'
            ],
            [
                'width'   => 50,
                'height'  => 50,
                'path'    => 'assets/img/pacotes/imagens/thumbs-sm/'
            ],
            [
                'width'   => 500,
                'height'  => 300,
                'path'    => 'assets/img/pacotes/imagens/'
            ],
            [
                'width'   => 980,
                'height'  => null,
                'path'    => 'assets/img/pacotes/imagens/ampliado/'
            ]
        ]);
    }
}
