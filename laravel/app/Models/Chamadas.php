<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Chamadas extends Model
{
    protected $table = 'chamadas';

    protected $guarded = ['id'];

    public static function upload_chamada_1_imagem()
    {
        return CropImage::make('chamada_1_imagem', [
            'width'  => 120,
            'height' => 120,
            'path'   => 'assets/img/chamadas/'
        ]);
    }

    public static function upload_chamada_2_imagem()
    {
        return CropImage::make('chamada_2_imagem', [
            'width'  => 120,
            'height' => 120,
            'path'   => 'assets/img/chamadas/'
        ]);
    }

}
