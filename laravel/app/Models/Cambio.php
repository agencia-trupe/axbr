<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Cambio extends Model
{
    protected $table = 'cambio';

    protected $guarded = ['id'];

}
