<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Cambio;
use App\Models\Chamadas;

class CambioController extends Controller
{
    public function index()
    {
        $cambio   = Cambio::first();
        $chamadas = Chamadas::first();

        return view('frontend.cambio', compact('cambio', 'chamadas'));
    }
}
