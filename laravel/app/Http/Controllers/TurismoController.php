<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests\CotacoesRecebidasRequest;

use App\Models\Servico;
use App\Models\Cruzeiro;
use App\Models\CruzeirosTexto;
use App\Models\Contato;
use App\Models\CotacaoRecebida;

class TurismoController extends Controller
{
    public function servicos()
    {
        $servicos = Servico::ordenados()->get();

        return view('frontend.servicos', compact('servicos'));
    }

    public function cruzeiros()
    {
        $cruzeiros = Cruzeiro::ordenados()->get();
        $texto     = CruzeirosTexto::first();
        $servicos  = Servico::ordenados()->get();

        return view('frontend.cruzeiros', compact('cruzeiros', 'texto', 'servicos'));
    }

    public function cruzeirosPost(CotacoesRecebidasRequest $request, CotacaoRecebida $contatoRecebido)
    {
        $input = $request->all();

        $contatoRecebido->create($input);

        $contato = Contato::first();

        if (isset($contato->email)) {
            \Mail::send('emails.cruzeiro', $input, function($message) use ($request, $contato)
            {
                $message->to($contato->email, 'AXBR')
                        ->subject('[CONTATO CRUZEIRO] AXBR')
                        ->replyTo($request->get('email'), $request->get('nome'));
            });
        }

        return back()->with('success', true);
    }
}
