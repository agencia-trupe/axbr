<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\PacotesImagensRequest;
use App\Http\Controllers\Controller;

use App\Models\Pacote;
use App\Models\PacoteImagem;

use App\Helpers\CropImage;

class PacotesImagensController extends Controller
{
    public function index(Pacote $registro)
    {
        $imagens = PacoteImagem::pacote($registro->id)->ordenados()->get();

        return view('painel.pacotes.imagens.index', compact('imagens', 'registro'));
    }

    public function show(Pacote $registro, PacoteImagem $imagem)
    {
        return $imagem;
    }

    public function store(Pacote $registro, PacotesImagensRequest $request)
    {
        try {

            $input = $request->all();
            $input['imagem'] = PacoteImagem::uploadImagem();
            $input['pacote_id'] = $registro->id;

            $imagem = PacoteImagem::create($input);

            $view = view('painel.pacotes.imagens.imagem', compact('registro', 'imagem'))->render();

            return response()->json(['body' => $view]);

        } catch (\Exception $e) {

            return 'Erro ao adicionar imagem: '.$e->getMessage();

        }
    }

    public function destroy(Pacote $registro, PacoteImagem $imagem)
    {
        try {

            $imagem->delete();
            return redirect()->route('painel.pacotes.imagens.index', $registro)
                             ->with('success', 'Imagem excluída com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir imagem: '.$e->getMessage()]);

        }
    }

    public function clear(Pacote $registro)
    {
        try {

            $registro->imagens()->delete();
            return redirect()->route('painel.pacotes.imagens.index', $registro)
                             ->with('success', 'Imagens excluídas com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir imagens: '.$e->getMessage()]);

        }
    }
}
