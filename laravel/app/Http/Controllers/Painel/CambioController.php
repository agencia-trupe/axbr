<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\CambioRequest;
use App\Http\Controllers\Controller;

use App\Models\Cambio;

class CambioController extends Controller
{
    public function index()
    {
        $registro = Cambio::first();

        return view('painel.cambio.edit', compact('registro'));
    }

    public function update(CambioRequest $request, Cambio $registro)
    {
        try {
            $input = $request->all();


            $registro->update($input);

            return redirect()->route('painel.cambio.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
