<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\CruzeirosTextoRequest;
use App\Http\Controllers\Controller;

use App\Models\CruzeirosTexto;

class CruzeirosTextoController extends Controller
{
    public function index()
    {
        $registro = CruzeirosTexto::first();

        return view('painel.cruzeiros-texto.edit', compact('registro'));
    }

    public function update(CruzeirosTextoRequest $request, CruzeirosTexto $registro)
    {
        try {
            $input = $request->all();


            $registro->update($input);

            return redirect()->route('painel.cruzeiros-texto.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
