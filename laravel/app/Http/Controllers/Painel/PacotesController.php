<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\PacotesRequest;
use App\Http\Controllers\Controller;

use App\Models\Pacote;

class PacotesController extends Controller
{
    public function index()
    {
        $registros = Pacote::ordenados()->get()->sortBy(function($pacote, $key) {
            return array_search($pacote->continente, array_keys(\Tools::continentes()));
        });

        return view('painel.pacotes.index', compact('registros'));
    }

    public function create()
    {
        $paises = Pacote::orderBy('pais', 'ASC')->select('pais')->distinct()->get();
        return view('painel.pacotes.create', compact('paises'));
    }

    public function store(PacotesRequest $request)
    {
        try {

            $input = $request->all();

            if (isset($input['capa'])) $input['capa'] = Pacote::upload_capa();

            Pacote::create($input);

            return redirect()->route('painel.pacotes.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Pacote $registro)
    {
        $paises = Pacote::orderBy('pais', 'ASC')->select('pais')->distinct()->get();
        return view('painel.pacotes.edit', compact('registro', 'paises'));
    }

    public function update(PacotesRequest $request, Pacote $registro)
    {
        try {

            $input = $request->all();

            if (isset($input['capa'])) $input['capa'] = Pacote::upload_capa();

            $registro->update($input);

            return redirect()->route('painel.pacotes.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Pacote $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.pacotes.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
