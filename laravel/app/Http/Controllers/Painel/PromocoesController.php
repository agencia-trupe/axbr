<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\PromocoesRequest;
use App\Http\Controllers\Controller;

use App\Models\Promocoes;
use App\Models\Pacote;

class PromocoesController extends Controller
{
    public function index()
    {
        $registro = Promocoes::first();

        $pacotes  = Pacote::ordenados()->get()->sortBy(function($pacote, $key) {
            return array_search($pacote->continente, array_keys(\Tools::continentes()));
        })->lists('titulo', 'id');

        return view('painel.promocoes.edit', compact('registro', 'pacotes'));
    }

    public function update(PromocoesRequest $request, Promocoes $registro)
    {
        try {
            $input = $request->all();

            $registro->update($input);

            return redirect()->route('painel.promocoes.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
