<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\CruzeirosRequest;
use App\Http\Controllers\Controller;

use App\Models\Cruzeiro;

class CruzeirosController extends Controller
{
    public function index()
    {
        $registros = Cruzeiro::ordenados()->get();

        return view('painel.cruzeiros.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.cruzeiros.create');
    }

    public function store(CruzeirosRequest $request)
    {
        try {

            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Cruzeiro::upload_imagem();

            Cruzeiro::create($input);

            return redirect()->route('painel.cruzeiros.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Cruzeiro $registro)
    {
        return view('painel.cruzeiros.edit', compact('registro'));
    }

    public function update(CruzeirosRequest $request, Cruzeiro $registro)
    {
        try {

            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Cruzeiro::upload_imagem();

            $registro->update($input);

            return redirect()->route('painel.cruzeiros.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Cruzeiro $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.cruzeiros.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
