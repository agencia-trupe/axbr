<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\InformacoesRequest;
use App\Http\Controllers\Controller;

use App\Models\Informacao;
use App\Models\Pacote;

class InformacoesController extends Controller
{
    public function index(Pacote $pacote)
    {
        $registros = Informacao::pacote($pacote->id)->ordenados()->get();

        return view('painel.informacoes.index', compact('registros', 'pacote'));
    }

    public function create(Pacote $pacote)
    {
        return view('painel.informacoes.create', compact('pacote'));
    }

    public function store(Pacote $pacote, InformacoesRequest $request)
    {
        try {

            $input = $request->all();
            $input['pacote_id'] = $pacote->id;

            Informacao::create($input);

            return redirect()->route('painel.pacotes.informacoes.index', $pacote->id)->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Pacote $pacote, Informacao $registro)
    {
        return view('painel.informacoes.edit', compact('registro', 'pacote'));
    }

    public function update(Pacote $pacote, InformacoesRequest $request, Informacao $registro)
    {
        try {

            $input = $request->all();

            $registro->update($input);

            return redirect()->route('painel.pacotes.informacoes.index', $pacote->id)->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Pacote $pacote, Informacao $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.pacotes.informacoes.index', $pacote->id)->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
