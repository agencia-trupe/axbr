<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\CotacaoRecebida;

class CotacoesRecebidasController extends Controller
{
    public function index()
    {
        $contatosrecebidos = CotacaoRecebida::orderBy('created_at', 'DESC')->get();

        return view('painel.contato.cotacoes.index', compact('contatosrecebidos'));
    }

    public function show(CotacaoRecebida $contato)
    {
        $contato->update(['lido' => 1]);

        return view('painel.contato.cotacoes.show', compact('contato'));
    }

    public function destroy(CotacaoRecebida $contato)
    {
        try {

            $contato->delete();
            return redirect()->route('painel.contato.cotacoes.index')->with('success', 'Mensagem excluída com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir mensagem: '.$e->getMessage()]);

        }
    }

    public function toggle(CotacaoRecebida $contato, Request $request)
    {
        try {

            $contato->update([
                'lido' => !$contato->lido
            ]);

            return redirect()->route('painel.contato.cotacoes.index')->with('success', 'Mensagem alterada com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar mensagem: '.$e->getMessage()]);

        }
    }
}
