<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ServicosRequest;
use App\Http\Controllers\Controller;

use App\Models\Servico;

class ServicosController extends Controller
{
    public function index()
    {
        $registros = Servico::ordenados()->get();

        return view('painel.servicos.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.servicos.create');
    }

    public function store(ServicosRequest $request)
    {
        try {

            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Servico::upload_imagem();

            Servico::create($input);

            return redirect()->route('painel.servicos.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Servico $registro)
    {
        return view('painel.servicos.edit', compact('registro'));
    }

    public function update(ServicosRequest $request, Servico $registro)
    {
        try {

            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Servico::upload_imagem();

            $registro->update($input);

            return redirect()->route('painel.servicos.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Servico $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.servicos.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
