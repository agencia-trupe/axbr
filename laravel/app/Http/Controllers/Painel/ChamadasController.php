<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ChamadasRequest;
use App\Http\Controllers\Controller;

use App\Models\Chamadas;

class ChamadasController extends Controller
{
    public function index()
    {
        $registro = Chamadas::first();

        return view('painel.chamadas.edit', compact('registro'));
    }

    public function update(ChamadasRequest $request, Chamadas $registro)
    {
        try {
            $input = $request->all();

            if (isset($input['chamada_1_imagem'])) $input['chamada_1_imagem'] = Chamadas::upload_chamada_1_imagem();
            if (isset($input['chamada_2_imagem'])) $input['chamada_2_imagem'] = Chamadas::upload_chamada_2_imagem();

            $registro->update($input);

            return redirect()->route('painel.chamadas.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
