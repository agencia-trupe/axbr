<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Cambio;
use App\Models\Chamadas;
use App\Models\Servico;
use App\Models\Promocoes;
use App\Models\Pacote;
use App\Models\Cruzeiro;

class HomeController extends Controller
{
    public function index()
    {
        $cambio    = Cambio::first();
        $chamadas  = Chamadas::first();
        $servicos  = Servico::ordenados()->get();
        $promocoes = Promocoes::first();
        $cruzeiros = Cruzeiro::ordenados()->get();

        $promocoes = [
            'nacionais'      => [$promocoes->nacional_1, $promocoes->nacional_2, $promocoes->nacional_3],
            'internacionais' => [$promocoes->internacional_1, $promocoes->internacional_2, $promocoes->internacional_3],
            'variaveis'      => [$promocoes->variavel_1, $promocoes->variavel_2, $promocoes->variavel_3, $promocoes->variavel_4],
            'variavelTitulo' => $promocoes->titulo_variavel
        ];

        foreach(['nacionais', 'internacionais', 'variaveis'] as $tipo) {
            $promocoes[$tipo] = array_filter(array_map(function($pacoteId) {
                return Pacote::find($pacoteId);
            }, $promocoes[$tipo]), function($pacote) {
                return $pacote;
            });
        }

        return view('frontend.home', compact('cambio', 'chamadas', 'servicos', 'promocoes', 'cruzeiros'));
    }
}
