<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests\ContatosRecebidosRequest;
use App\Http\Controllers\Controller;

use App\Models\Contato;
use App\Models\ContatoRecebido;

class ContatoController extends Controller
{
    public function index()
    {
        $contato = Contato::first();

        return view('frontend.contato', compact('contato'));
    }

    public function post(ContatosRecebidosRequest $request, ContatoRecebido $contatoRecebido)
    {
        $input = $request->all();

        $contatoRecebido->create($input);

        $contato = Contato::first();

        if (isset($contato->email)) {
            \Mail::send('emails.contato', $input, function($message) use ($request, $contato)
            {
                $message->to($contato->email, 'AXBR')
                        ->subject('[CONTATO] AXBR')
                        ->replyTo($request->get('email'), $request->get('nome'));
            });
        }

        return back()->with('success', true);
    }
}
