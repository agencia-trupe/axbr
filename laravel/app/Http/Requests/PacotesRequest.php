<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class PacotesRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'continente' => 'required',
            'pais' => 'required',
            'titulo' => 'required',
            'subtitulo' => '',
            'capa' => 'required|image',
            'texto' => 'required',
        ];

        if ($this->method() != 'POST') {
            $rules['capa'] = 'image';
        }

        return $rules;
    }
}
