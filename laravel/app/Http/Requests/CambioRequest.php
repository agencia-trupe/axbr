<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class CambioRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'data'                     => 'required',
            'frase'                    => 'required',
            'dolar_americano_compra'   => 'required|numeric|between:0.00,9999.99',
            'dolar_americano_venda'    => 'required|numeric|between:0.00,9999.99',
            'euro_compra'              => 'required|numeric|between:0.00,9999.99',
            'euro_venda'               => 'required|numeric|between:0.00,9999.99',
            'dolar_australiano_compra' => 'required|numeric|between:0.00,9999.99',
            'dolar_australiano_venda'  => 'required|numeric|between:0.00,9999.99',
            'dolar_canadense_compra'   => 'required|numeric|between:0.00,9999.99',
            'dolar_canadense_venda'    => 'required|numeric|between:0.00,9999.99',
            'dolar_neozelandes_compra' => 'required|numeric|between:0.00,9999.99',
            'dolar_neozelandes_venda'  => 'required|numeric|between:0.00,9999.99',
            'peso_argentino_compra'    => 'required|numeric|between:0.00,9999.99',
            'peso_argentino_venda'     => 'required|numeric|between:0.00,9999.99',
            'peso_chileno_compra'      => 'required|numeric|between:0.00,9999.99',
            'peso_chileno_venda'       => 'required|numeric|between:0.00,9999.99',
            'peso_colombiano_compra'   => 'required|numeric|between:0.00,9999.99',
            'peso_colombiano_venda'    => 'required|numeric|between:0.00,9999.99',
            'peso_mexicano_compra'     => 'required|numeric|between:0.00,9999.99',
            'peso_mexicano_venda'      => 'required|numeric|between:0.00,9999.99',
            'peso_uruguaio_compra'     => 'required|numeric|between:0.00,9999.99',
            'peso_uruguaio_venda'      => 'required|numeric|between:0.00,9999.99',
            'libra_esterlina_compra'   => 'required|numeric|between:0.00,9999.99',
            'libra_esterlina_venda'    => 'required|numeric|between:0.00,9999.99',
            'franco_suisso_compra'     => 'required|numeric|between:0.00,9999.99',
            'franco_suisso_venda'      => 'required|numeric|between:0.00,9999.99',
            'novo_sol_peruano_compra'  => 'required|numeric|between:0.00,9999.99',
            'novo_sol_peruano_venda'   => 'required|numeric|between:0.00,9999.99',
            'iene_japones_compra'      => 'required|numeric|between:0.00,9999.99',
            'iene_japones_venda'       => 'required|numeric|between:0.00,9999.99',
        ];
    }
}
