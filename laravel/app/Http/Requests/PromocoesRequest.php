<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class PromocoesRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'nacional_1' => '',
            'nacional_2' => '',
            'nacional_3' => '',
            'internacional_1' => '',
            'internacional_2' => '',
            'internacional_3' => '',
            'titulo_variavel' => 'required',
            'variavel_1' => '',
            'variavel_2' => '',
            'variavel_3' => '',
            'variavel_4' => '',
        ];
    }
}
