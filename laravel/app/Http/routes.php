<?php

Route::group(['middleware' => ['web']], function () {
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('cambio', 'CambioController@index')->name('cambio');
    Route::get('turismo/servicos', 'TurismoController@servicos')->name('turismo.servicos');
    Route::get('turismo/cruzeiros', 'TurismoController@cruzeiros')->name('turismo.cruzeiros');
    Route::post('turismo/cruzeiros', 'TurismoController@cruzeirosPost')->name('turismo.cruzeiros-post');
    Route::get('turismo/{continente?}/{pais?}/{pacote?}', 'TurismoController@index')->name('turismo');
    Route::get('contato', 'ContatoController@index')->name('contato');
    Route::post('contato', 'ContatoController@post')->name('contato.post');

    // Painel
    Route::group([
        'prefix'     => 'painel',
        'namespace'  => 'Painel',
        'middleware' => ['auth']
    ], function() {
        Route::get('/', 'PainelController@index')->name('painel');

        /* GENERATED ROUTES */
		Route::resource('promocoes', 'PromocoesController', ['only' => ['index', 'update']]);
        Route::resource('pacotes', 'PacotesController');
        Route::get('pacotes/{pacotes}/imagens/clear', [
            'as'   => 'painel.pacotes.imagens.clear',
            'uses' => 'PacotesImagensController@clear'
        ]);
        Route::resource('pacotes.imagens', 'PacotesImagensController', ['parameters' => ['imagens' => 'imagens_pacotes']]);
		Route::resource('pacotes.informacoes', 'InformacoesController');
		Route::resource('chamadas', 'ChamadasController', ['only' => ['index', 'update']]);
		Route::resource('cambio', 'CambioController', ['only' => ['index', 'update']]);
		Route::resource('cruzeiros-texto', 'CruzeirosTextoController', ['only' => ['index', 'update']]);
		Route::resource('cruzeiros', 'CruzeirosController');
		Route::resource('servicos', 'ServicosController');
		Route::resource('configuracoes', 'ConfiguracoesController', ['only' => ['index', 'update']]);

        Route::get('contato/recebidos/{recebidos}/toggle', ['as' => 'painel.contato.recebidos.toggle', 'uses' => 'ContatosRecebidosController@toggle']);
        Route::resource('contato/recebidos', 'ContatosRecebidosController');
        Route::get('contato/cotacoes/{cotacoes}/toggle', ['as' => 'painel.contato.cotacoes.toggle', 'uses' => 'CotacoesRecebidasController@toggle']);
        Route::resource('contato/cotacoes', 'CotacoesRecebidasController');
        Route::resource('contato', 'ContatoController');
        Route::resource('usuarios', 'UsuariosController');

        Route::post('ckeditor-upload', 'PainelController@imageUpload');
        Route::post('order', 'PainelController@order');
        Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

        Route::get('generator', 'GeneratorController@index')->name('generator.index');
        Route::post('generator', 'GeneratorController@submit')->name('generator.submit');
    });

    // Auth
    Route::group([
        'prefix'    => 'painel',
        'namespace' => 'Auth'
    ], function() {
        Route::get('login', 'AuthController@showLoginForm')->name('auth');
        Route::post('login', 'AuthController@login')->name('login');
        Route::get('logout', 'AuthController@logout')->name('logout');
    });
});
